<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@dashboard')->name('dashboard');

//Clients
Route::get('/clients/client-list', 'ClientController@clientList');
Route::get('/clients/add-client', 'ClientController@addClient');
Route::post('/clients/add-client/create', 'ClientController@createClient');
Route::get('/clients/edit-client/{clientId}', 'ClientController@editClient');
Route::post('/clients/edit-client/{clientId}/update', 'ClientController@updateClient');
Route::post('/clients/client/{clientId}/delete', 'ClientController@deleteClient');

//Company
Route::get('/companies/company-list', 'CompanyController@companyList');
Route::get('/companies/client/{clientId}', 'CompanyController@clientCompanyList');
Route::get('/companies/add-company/client/{clientId}', 'CompanyController@addCompany');
Route::post('/companies/add-company/client/{clientId}/create', 'CompanyController@createCompany');
Route::get('/companies/company/{companyId}', 'CompanyController@companySingle');
Route::post('/companies/company/{companyId}/update', 'CompanyController@updateCompany');
Route::post('/companies/company/{companyId}/suspend', 'CompanyController@suspendCompany');
Route::post('/companies/company/{companyId}/delete', 'CompanyController@deleteCompany');
