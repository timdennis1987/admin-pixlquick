@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-3 justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3>Hey {{ \Auth::user()->name }}</h3>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <i class="fas fa-envelope fa-lg mx-2"></i>You have 3 new <a href="/messages">messages</a>
                        <br><br>
                        <i class="fas fa-user-plus fa-lg mx-2"></i>Add a new <a href="/clients/add-client">client</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
