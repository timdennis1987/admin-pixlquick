@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row my-3">
            <div class="col-6">
                <h3>
                    Company List
                </h3>
            </div>
        </div>

        <div class="row">

            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">URL</th>
                    <th scope="col">Package</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <th scope="row">
                            <a href="/companies/company/{{ $company->id }}">
                                {{ $company->company_name }}
                            </a>
                        </th>
                        <td>{{ $company->email_1 }}</td>
                        <td>
                            <a href="{{ $company->url_1 }}" target="_blank">
                                {{ $company->url_1 }}
                            </a>
                        </td>
                        <td>{{ $company->name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
