@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row my-3">
            <div class="col-6 bread-crumb">
                <div class="float-left">
                    <a href="/clients/client-list">Clients</a> /
                    {{ $client->first_name }} {{ $client->last_name }}
                </div>
            </div>
        </div>

        <div class="row">

            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Personal Email</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">
                            {{ $client->first_name }}
                        </th>
                        <td>
                            {{ $client->last_name }}
                        </td>
                        <td>
                            {{ $client->email_personal }}
                        </td>
                        <td>
                            {{ $client->phone }}
                        </td>
                        <td class="text-center">
                            <a href="/clients/edit-client/{{ $client->client_id }}">
                                <i class="far fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>

            <a href="/companies/add-company/client/{{ $client->client_id }}" class="btn btn-primary my-3">
                Add company
            </a>

            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">Company Name</th>
                    <th scope="col">Company Email</th>
                    <th scope="col">Company URL</th>
                    <th scope="col">Package</th>
                </tr>
                </thead>
                <tbody>
                @foreach($companies as $company)
                    <tr>
                        <th scope="row">
                            <a href="/companies/company/{{ $company->id }}">
                                {{ $company->company_name }}
                            </a>
                        </th>
                        <td>
                            {{ $company->email_1 }}
                        </td>
                        <td>
                            <a href="{{ $company->url_1 }}" target="_blank">
                                {{ $company->url_1 }}
                            </a>
                        </td>
                        <td>
                            {{ $company->name }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
