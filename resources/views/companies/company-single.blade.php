@extends('layouts.app')
@section('content')

    @if($company->suspended_date != NULL)
        <div class="alert alert-warning text-center" role="alert">
            Account suspended on {{ \Carbon\Carbon::parse($company->suspended_date)->format('d/m/Y') }}
        </div>
    @endif

    @if($company->deleted_date != NULL)
        <div class="alert alert-danger text-center" role="alert">
            Account deleted on {{ \Carbon\Carbon::parse($company->deleted_date)->format('d/m/Y') }}
        </div>
    @endif

    <div class="container">

        <div class="row my-3">
            <div class="col-6 bread-crumb">
                <div class="float-left">
                    <a href="/clients/client-list">Clients</a> /
                    <a href="/companies/client/{{ $client->client_id }}">{{ $client->first_name }} {{ $client->last_name }}</a> /
                    {{ $company->company_name }}
                </div>
            </div>
            <div class="col-6">
                <div class="float-right">
                    <div class="row">
                        <form action="/companies/company/{{ $company->id }}/suspend" method="post">
                            @csrf
                            @if($company->suspended_date == NULL)
                                <input type="hidden" name="is_suspended" value="0">
                                <button type="submit" class="btn btn-warning">Suspend</button>
                            @else
                                <input type="hidden" name="is_suspended" value="1">
                                <button type="submit" class="btn btn-primary">Un-suspend</button>
                            @endif
                        </form>

                        <form action="/companies/company/{{ $company->id }}/delete" method="post">
                            @csrf
                            <button type="submit" class="btn btn-danger ml-3">Delete</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <form action="/companies/company/{{ $company->id }}/update" method="post">
            @csrf

            <h3 class="mt-5 mb-3">
                Owner details
            </h3>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="text" class="form-control" value="{{ $client->first_name }}" disabled>
                </div>

                <div class="form-group col-md-6">
                    <input type="text" class="form-control" value="{{ $client->last_name }}" disabled>
                </div>
            </div>

            <hr>

            <h3 class="mt-5 mb-3">
                Company details
            </h3>

            <div class="form-row">
                <div class="form-group col-md-10">
                    <label for="company_name">Company Name</label>
                    <input type="text" class="form-control" id="company_name" name="company_name"
                           value="{{ $company->company_name }}">
                </div>

                <div class="form-group col-md-2">
                    <label for="brand_color">Brand Colour</label>
                    <input type="text" class="form-control" id="brand_color" name="brand_color"
                           value="{{ $company->brand_color }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="email_1">Main Email</label>
                    <input type="text" class="form-control" id="email_1" name="email_1" value="{{ $company->email_1 }}">
                </div>

                <div class="form-group col-md-6">
                    <label for="email_2">Secondary Email</label>
                    <input type="text" class="form-control" id="email_2" name="email_2" value="{{ $company->email_2 }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="address_1">Address 1</label>
                    <input type="text" class="form-control" id="address_1" name="address_1"
                           value="{{ $company->address_1 }}">
                </div>

                <div class="form-group col-md-6">
                    <label for="address_2">Address 2</label>
                    <input type="text" class="form-control" id="address_2" name="address_2"
                           value="{{ $company->address_2 }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="city">City</label>
                    <input type="text" class="form-control" id="city" name="city" value="{{ $company->city }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="county">County</label>
                    <input type="text" class="form-control" id="county" name="county" value="{{ $company->county }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="country">Country</label>
                    <input type="text" class="form-control" id="country" name="country" value="{{ $company->country }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="postcode">Postcode</label>
                    <input type="text" class="form-control" id="postcode" name="postcode"
                           value="{{ $company->postcode }}">
                </div>
            </div>

            <hr>

            <h3 class="mt-5 mb-3">
                Website details
            </h3>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="url_1">Main domain</label>
                    <input type="text" class="form-control" id="url_1" name="url_1" value="{{ $company->url_1 }}">
                </div>

                <div class="form-group col-md-6">
                    <label for="url_2">Secondary domain</label>
                    <input type="text" class="form-control" id="url_2" name="url_2" value="{{ $company->url_2 }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="keyword_1">Keyword 1</label>
                    <input type="text" class="form-control" id="keyword_1" name="keyword_1"
                           value="{{ $company->keyword_1 }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="keyword_2">Keyword 2</label>
                    <input type="text" class="form-control" id="keyword_2" name="keyword_2"
                           value="{{ $company->keyword_2 }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="keyword_3">Keyword 3</label>
                    <input type="text" class="form-control" id="keyword_3" name="keyword_3"
                           value="{{ $company->keyword_3 }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="keyword_4">Keyword 4</label>
                    <input type="text" class="form-control" id="keyword_4" name="keyword_4"
                           value="{{ $company->keyword_4 }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="website_package">Website package</label>
                    <select class="form-control" id="website_package" name="website_package">
                        @foreach($packages as $package)
                            <option
                                value="{{ $package->id }}" {{ $company->website_package == $package->id ? 'selected' : '' }}>
                                {{ $package->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="seo">SEO</label>
                    <select class="form-control" id="seo" name="seo">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="signed_off">Signed off</label>
                    <select class="form-control" id="signed_off" name="signed_off">
                        <option value="0" {{ $company->signed_off == 0 ? 'selected' : '' }}>No</option>
                        <option value="1" {{ $company->signed_off == 1 ? 'selected' : '' }}>Yes</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="live">Live</label>
                    <select class="form-control" id="live" name="live">
                        <option value="0" {{ $company->live == 0 ? 'selected' : '' }}>No</option>
                        <option value="1" {{ $company->live == 1 ? 'selected' : '' }}>Yes</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="amount">Quote</label>
                    <input type="text" class="form-control" id="amount" name="amount" value="{{ $quote->amount }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="payed">Payed</label>
                    <input type="text" class="form-control" id="payed" name="payed" value="{{ $quote->payed }}">
                </div>
                <div class="form-group col-md-3">
                    <label for="remaining_amount">Remaining</label>
                    <input type="text" class="form-control" id="remaining_amount"
                           value="{{ $quote->amount - $quote->payed }}" disabled>
                </div>
                <div class="form-group col-md-3">
                    <label for="per_month">Monthly subscription</label>
                    <input type="text" class="form-control" id="per_month" name="per_month"
                           value="{{ $quote->per_month }}">
                </div>
            </div>

            <hr>

            <h3 class="mt-5 mb-3">
                Company notes
            </h3>

            <div class="form-group">
                <textarea class="form-control" id="company_notes" name="company_notes"
                          rows="3">{!! $company->company_notes !!}</textarea>
            </div>

            <div class="mt-3 mb-5">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>

        </form>

    </div>

@endsection
