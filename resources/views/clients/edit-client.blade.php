@extends('layouts.app')
@section('content')

    @if($client->deleted_date != NULL)
        <div class="alert alert-danger text-center" role="alert">
            Client deleted on {{ \Carbon\Carbon::parse($client->deleted_date)->format('d/m/Y') }}
        </div>
    @endif

    <div class="container">

        <div class="row my-3">
            <div class="col-6 bread-crumb">
                <div class="float-left">
                    <a href="/clients/client-list">Clients</a> /
                    <a href="/companies/client/{{ $client->client_id }}">{{ $client->first_name }} {{ $client->last_name }}</a> /
                    Edit client
                </div>
            </div>
            <div class="col-6">
                <div class="float-right">
                    <form action="/clients/client/{{ $client->client_id }}/delete" method="post">
                        @csrf
                        <button type="submit" class="btn btn-danger ml-3">Delete</button>
                    </form>
                </div>
            </div>
        </div>

        <form action="/clients/edit-client/{{ $client->client_id }}/update" method="post">
            @csrf

            <h3 class="mt-5 mb-3">
                Client details
            </h3>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="first_name">First name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $client->first_name }}">
                </div>

                <div class="form-group col-md-6">
                    <label for="last_name">Last name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $client->last_name }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="email_personal">Personal email</label>
                    <input type="text" class="form-control" id="email_personal" name="email_personal" value="{{ $client->email_personal }}">
                </div>

                <div class="form-group col-md-4">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $client->phone }}">
                </div>

                <div class="form-group col-md-4">
                    <label for="password">Password</label>
                    <input type="text" class="form-control" id="password" name="password" value="{{ $client->password }}">
                </div>
            </div>

            <hr>

            <h3 class="mt-5 mb-3">
                Client notes
            </h3>

            <div class="form-group">
                <textarea class="form-control" id="client_notes" name="client_notes" rows="3">{{ $client->client_notes }}</textarea>
            </div>

            <div class="mt-3 mb-5">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>

        </form>

    </div>

@endsection
