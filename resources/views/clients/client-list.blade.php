@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row my-3">
            <div class="col-6">
                <h3>
                    Client List
                </h3>
            </div>
            <div class="col-6">
                <div class="float-right">
                    <a href="/clients/add-client" class="btn btn-primary">
                        Add client
                    </a>
                </div>
            </div>
        </div>

        <div class="row">

            <table class="table table-dark">
                <thead>
                <tr>
                    <th scope="col">Email</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col" class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr>
                        <th scope="row">
                            <a href="/companies/client/{{ $client->client_id }}">
                                {{ $client->email_personal }}
                            </a>
                        </th>
                        <td>{{ $client->first_name }}</td>
                        <td>{{ $client->last_name }}</td>
                        <td>{{ $client->phone }}</td>
                        <td class="text-center">
                            <a href="/clients/edit-client/{{ $client->client_id }}">
                                <i class="far fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
