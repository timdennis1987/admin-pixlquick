@extends('layouts.app')
@section('content')

    <div class="container">

        <div class="row my-3">
            <div class="col-6 bread-crumb">
                <div class="float-left">
                    <a href="/clients/client-list">Clients</a> /
                    Create client
                </div>
            </div>
        </div>

        <form action="/clients/add-client/create" method="post">
            @csrf

            <h3 class="mt-5 mb-3">
                Client details
            </h3>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="first_name">First name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name">
                </div>

                <div class="form-group col-md-6">
                    <label for="last_name">Last name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="email_personal">Personal email</label>
                    <input type="text" class="form-control" id="email_personal" name="email_personal">
                </div>

                <div class="form-group col-md-4">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>

                <div class="form-group col-md-4">
                    <label for="password">Password</label>
                    <input type="text" class="form-control" id="password" name="password">
                </div>
            </div>

            <hr>

            <h3 class="mt-5 mb-3">
                Client notes
            </h3>

            <div class="form-group">
                <textarea class="form-control" id="client_notes" name="client_notes" rows="3"></textarea>
            </div>

            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="add_company" name="add_company" value="1">
                <label class="form-check-label" for="add_company">Add company for client</label>
            </div>

            <div class="mt-3 mb-5">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>

        </form>

    </div>

@endsection
