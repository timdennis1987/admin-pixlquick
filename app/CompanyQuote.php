<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyQuote extends Model
{
    protected $table = 'company_quotes';

    public $timestamps = false;

    protected $guarded = [];
}
