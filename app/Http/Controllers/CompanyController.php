<?php

namespace App\Http\Controllers;

use App\Client;
use App\Company;
use App\CompanyQuote;
use App\Package;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function companyList()
    {
        $companies = Company::join('packages', 'packages.id', '=', 'companies.website_package')
            ->where('companies.deleted_date', NULL)
            ->get([
                'packages.name',
                'companies.id',
                'companies.suspended_date',
                'companies.live',
                'companies.company_name',
                'companies.email_1',
                'companies.url_1',
            ]);

        return view('companies.company-list', [
            'companies' => $companies
        ]);
    }

    public function clientCompanyList($clientId)
    {
        $client = Client::where('client_id', $clientId)->first();

        $companies = Company::join('packages', 'packages.id', '=', 'companies.website_package')
            ->where('companies.client_id', $clientId)
            ->where('companies.deleted_date', NULL)
            ->get([
                'packages.name',
                'companies.id',
                'companies.suspended_date',
                'companies.live',
                'companies.company_name',
                'companies.email_1',
                'companies.url_1',
            ]);

        return view('companies.client-companies', [
            'client' => $client,
            'companies' => $companies
        ]);
    }

    public function addCompany($clientId)
    {
        $client = Client::where('client_id', $clientId)->first();
        $packages = Package::get(['id','name']);

        return view('companies.add-company', [
            'client' => $client,
            'packages' => $packages
        ]);
    }

    public function createCompany(Request $request, $clientId)
    {
        Company::create([
            'client_id' => $clientId,
            'company_name' => $request->company_name,
            'brand_color' => $request->brand_color,
            'email_1' => $request->email_1,
            'email_2' => $request->email_2,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'city' => $request->city,
            'county' => $request->county,
            'country' => $request->country,
            'postcode' => $request->postcode,
            'url_1' => $request->url_1,
            'url_2' => $request->url_2,
            'keyword_1' => $request->keyword_1,
            'keyword_2' => $request->keyword_2,
            'keyword_3' => $request->keyword_3,
            'keyword_4' => $request->keyword_4,
            'website_package' => $request->website_package,
            'seo' => $request->seo,
            'signed_off' => $request->signed_off,
            'live' => $request->live,
            'company_notes' => $request->company_notes,
        ]);

        $company = Company::where('client_id', $clientId)->orderBy('id', 'desc')->first();

        $complete = 0;

        if($request->amount == $request->payed) {
            $complete = 1;
        }

        CompanyQuote::create([
            'company_id' => $company->id,
            'amount' => $request->amount,
            'payed' => $request->payed,
            'complete' => $complete,
            'per_month' => $request->per_month,
        ]);

        return redirect('/companies/client/' . $clientId);
    }

    public function companySingle($companyId)
    {
        $company = Company::where('companies.id', $companyId)->first();
        $client = Client::where('client_id', $company->client_id)->first();
        $quote = CompanyQuote::where('company_id', $companyId)->first();
        $packages = Package::get(['id','name']);

        return view('companies.company-single', [
            'company' => $company,
            'client' => $client,
            'quote' => $quote,
            'packages' => $packages
        ]);
    }

    public function updateCompany(Request $request, $companyId)
    {
        $company = Company::where('companies.id', $companyId)->first();
        $quote = CompanyQuote::where('company_id', $companyId)->first();

        $company->update([
            'company_name' => $request->company_name,
            'brand_color' => $request->brand_color,
            'email_1' => $request->email_1,
            'email_2' => $request->email_2,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'city' => $request->city,
            'county' => $request->county,
            'country' => $request->country,
            'postcode' => $request->postcode,
            'url_1' => $request->url_1,
            'url_2' => $request->url_2,
            'keyword_1' => $request->keyword_1,
            'keyword_2' => $request->keyword_2,
            'keyword_3' => $request->keyword_3,
            'keyword_4' => $request->keyword_4,
            'website_package' => $request->website_package,
            'seo' => $request->seo,
            'signed_off' => $request->signed_off,
            'live' => $request->live,
            'company_notes' => $request->company_notes,
        ]);

        $complete = 0;

        if($request->amount == $request->payed) {
            $complete = 1;
        }

        $quote->update([
            'amount' => $request->amount,
            'payed' => $request->payed,
            'complete' => $complete,
            'per_month' => $request->per_month,
        ]);

        return redirect('/companies/client/' . $company->client_id);
    }

    public function suspendCompany(Request $request, $companyId)
    {
        $company = Company::where('companies.id', $companyId)->first();

        if($request->is_suspended == 1){
            $suspended = NULL;
        } else {
            $suspended = \Carbon\Carbon::now();
        }

        $company->update([
            'suspended_date' => $suspended
        ]);

        return redirect()->back();
    }

    public function deleteCompany(Request $request, $companyId)
    {
        $company = Company::where('companies.id', $companyId)->first();

        $company->update([
            'deleted_date' => \Carbon\Carbon::now()
        ]);

        return redirect()->back();
    }
}
