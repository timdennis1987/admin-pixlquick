<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function clientList()
    {
        $clients = Client::where('deleted_date', NULL)->get();

        return view('clients.client-list', [
            'clients' => $clients
        ]);
    }

    public function addClient()
    {
        return view('clients.add-client');
    }

    public function createClient(Request $request)
    {
        $lastClient = Client::orderBy('client_id', 'desc')->first();

        $clientId = $lastClient->client_id +1;

        Client::create([
            'client_id' => $clientId,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email_personal' => $request->email_personal,
            'phone' => $request->phone,
            'password' => $request->password,
            'client_notes' => $request->client_notes
        ]);

        if($request->add_company == 1) {
            return redirect('/companies/add-company/client/' . $clientId);
        }

        return redirect('/companies/client/' . $clientId);
    }

    public function editClient($clientId)
    {
        $client = Client::where('client_id', $clientId)->first();

        return view('clients.edit-client', [
            'client' => $client
        ]);
    }

    public function updateClient(Request $request, $clientId)
    {
        $client = Client::where('client_id', $clientId)->first();

        $client->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email_personal' => $request->email_personal,
            'phone' => $request->phone,
            'password' => $request->password,
            'client_notes' => $request->client_notes
        ]);

        return redirect('/companies/client/' . $clientId);
    }

    public function deleteClient($clientId)
    {
        $client = Client::where('client_id', $clientId)->first();

        $client->update([
            'deleted_date' => \Carbon\Carbon::now()
        ]);

        return redirect()->back();
    }
}
