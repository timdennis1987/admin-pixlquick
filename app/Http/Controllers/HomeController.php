<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $user = auth()->user()->id;

        if (!$user) {
            return view('auth.login');
        }

        return view('dashboard',[
            'user' => $user
        ]);
    }
}
